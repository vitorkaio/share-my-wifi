export interface Wifi {
  key: string
  ssid: string
  password: string
  description: string
  timer: number
  updateAt: Date
}
