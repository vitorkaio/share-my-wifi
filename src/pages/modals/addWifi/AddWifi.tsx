import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modal'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {
  Container,
  Header,
  Content,
  BackButton,
  HeaderTitle
} from './AddWifiStyles'
import { StateProps, DispatchProps } from './AddWifiTypes'
import { ADD_WIFI_FIELDS } from 'pages/home/content/creates/CreatesConstants'
import RText from 'components/ui/rtext/RText'
import { textPrimary } from 'components/styles/Colors'

type Props = StateProps & DispatchProps

const AddWifi: React.FC<Props> = ({
  visible,
  close,
  field,
  incIndex,
  descIndex
}) => {
  return (
    <Modal
      isVisible={visible}
      onBackButtonPress={() => close()}
      animationIn="slideInRight"
      animationOut="slideOutLeft"
    >
      <Container>
        <Header>
          <BackButton onPress={() => descIndex()}>
            <Icon name="arrow-left-thick" size={26} color={textPrimary} />
          </BackButton>
          <HeaderTitle>
            <RText
              text={field.text}
              size={18}
              family="Oswald-SemiBold"
              color={textPrimary}
            />
          </HeaderTitle>
        </Header>
        <Content>
          <TouchableOpacity onPress={() => incIndex()}>
            <Text>Continuar</Text>
          </TouchableOpacity>
          {field.key === ADD_WIFI_FIELDS.SSID ? <Text>Wifis perto</Text> : null}
        </Content>
      </Container>
    </Modal>
  )
}

export default AddWifi

/* import Icon from 'react-native-vector-icons/FontAwesome'
<Icon name="rocket" size={30} color="#900" /> */
