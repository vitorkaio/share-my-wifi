import styled from 'styled-components/native'
import { backgroundColor } from 'components/styles/Colors'

export const Container = styled.View`
  flex: 1;
  background-color: ${backgroundColor};
`

export const Header = styled.View`
  height: 90px;
`

export const BackButton = styled.TouchableOpacity`
  flex: 2;
  justify-content: flex-end;
  align-items: flex-start;
  padding-left: 15px;
`

export const HeaderTitle = styled.View`
  flex: 3;
  justify-content: center;
  align-items: center;
`

export const Content = styled.View`
  flex: 1;
`
