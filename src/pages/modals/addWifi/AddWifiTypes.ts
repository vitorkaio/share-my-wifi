import { AddWifiModalFields } from 'pages/home/content/creates/CreatesConstants'

export interface StateProps {
  visible: boolean
  field: AddWifiModalFields
}

export interface DispatchProps {
  close(): void
  incIndex(): void
  descIndex(): void
}
