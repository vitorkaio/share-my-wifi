export enum ADD_WIFI_FIELDS {
  SSID = 1,
  PASSWORD = 2,
  DESCRIPTION = 3,
  TIMER = 4,
  QRCODE = 5
}

export interface AddWifiModalFields {
  key: number
  text: string
  type: string
}

export const ListAddWifiModalFields: Array<AddWifiModalFields> = [
  {
    key: ADD_WIFI_FIELDS.SSID,
    text: 'Insira o SSID da rede wifi',
    type: 'text'
  },
  {
    key: ADD_WIFI_FIELDS.PASSWORD,
    text: 'A senha da rede wifi: ',
    type: 'text'
  },
  {
    key: ADD_WIFI_FIELDS.DESCRIPTION,
    text: 'Uma descrição para a rede',
    type: 'text'
  },
  {
    key: ADD_WIFI_FIELDS.TIMER,
    text: 'Defina o tempo de vida útil para a rede',
    type: 'number'
  },
  {
    key: ADD_WIFI_FIELDS.QRCODE,
    text: 'Aqui está o QRCODE com os dados da rede',
    type: 'text'
  }
]
