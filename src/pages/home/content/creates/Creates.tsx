import React, { useState } from 'react'
import { Text } from 'react-native'

import { Container } from './CreatesStyles'
import Add from 'components/ui/addButton/AddButton'
import AddWifiModal from 'pages/modals/addWifi/AddWifi'

import { ListAddWifiModalFields } from './CreatesConstants'

const Creates: React.FC = () => {
  const [addModalIsVisible, setAddModalIsVisible] = useState<boolean>(false)
  const [indexAddWifiFields, setIndexAddWifiFields] = useState(0)

  const addWifi = () => {
    console.log('Add WiFi')
    setAddModalIsVisible(true)
  }

  const closeModal = () => {
    setIndexAddWifiFields(0)
    setAddModalIsVisible(false)
    return null
  }

  const descIndexHandler = () => {
    if (indexAddWifiFields > 0) {
      setIndexAddWifiFields(indexAddWifiFields - 1)
    } else {
      closeModal()
    }
  }

  const renderAddWifiModalfields = () => {
    if (indexAddWifiFields <= ListAddWifiModalFields.length - 1) {
      return (
        <AddWifiModal
          key={ListAddWifiModalFields[indexAddWifiFields].key}
          field={ListAddWifiModalFields[indexAddWifiFields]}
          incIndex={() => setIndexAddWifiFields(indexAddWifiFields + 1)}
          descIndex={descIndexHandler}
          visible={addModalIsVisible}
          close={closeModal}
        />
      )
    } else {
      // addModalHandler()
      closeModal()
    }
  }

  return (
    <Container>
      <Text>Container Creates</Text>
      <Add onPressHandler={addWifi} />
      {renderAddWifiModalfields()}
    </Container>
  )
}

export default Creates
