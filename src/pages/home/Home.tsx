import React, { useState } from 'react'
import { Text } from 'react-native'

import { Container } from './HomeStyles'
import NavigateTab from 'components/navigatetab/NavigateTab'
import CreatesContainer from 'pages/home/content/creates/Creates'

const Home: React.FC = () => {
  const [index, setIndex] = useState(1)

  const changeTab = (newIndex: number) => {
    setIndex(newIndex)
  }

  const renderContainer = () => {
    if (index === 1) {
      return <CreatesContainer />
    } else {
      return <Text>Container B</Text>
    }
  }

  return (
    <Container>
      <NavigateTab tab={changeTab} currentTab={index} />
      {renderContainer()}
    </Container>
  )
}

export default Home
