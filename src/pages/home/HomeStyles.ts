import styled from 'styled-components/native'
import { backgroundColor } from 'components/styles/Colors'

export const Container = styled.View`
  flex: 1;
  justify-content: flex-start;
  align-items: stretch;
  background-color: ${backgroundColor};
`
