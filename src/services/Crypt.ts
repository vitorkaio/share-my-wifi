import CryptoJS from 'crypto-js'

const PASS_PHRASE = 'amendas29-09'

// Criptografa e descriptografa os dados que serão salvo no banco
class Crypt {
  static cryptData(data: string): string {
    return CryptoJS.AES.encrypt(data, PASS_PHRASE).toString()
  }

  static decryptData(dataCrypt: string): string {
    const bytes = CryptoJS.AES.decrypt(dataCrypt, PASS_PHRASE)
    const originalText = bytes.toString(CryptoJS.enc.Utf8)
    return originalText
  }
}

export default Crypt
