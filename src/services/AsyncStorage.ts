import AsyncStorage from '@react-native-community/async-storage'
import { Wifi } from 'models/Wifi'

class DatabaseAsyncStorage {
  static async setStoreData(data: Wifi): Promise<boolean> {
    try {
      const jsonValue = JSON.stringify(data)
      await AsyncStorage.setItem(data.key, jsonValue)
      return true
    } catch (err) {
      throw err.message
    }
  }

  static async getStoreData(key: string): Promise<Wifi | undefined> {
    try {
      const jsonValue = await AsyncStorage.getItem(key)
      if (jsonValue) {
        const data: Wifi = JSON.parse(jsonValue)
        return data
      }
      return undefined
    } catch (err) {
      throw err.message
    }
  }
}

export default DatabaseAsyncStorage
