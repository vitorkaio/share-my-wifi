import React from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'

import { Container } from './NavigateTabStyles'
import { StateProps, DispatchProps } from './NavigateTabTypes'
import { FONTS_STATUS, TAB_NAMES } from './NavigateConstants'

import RText from 'components/ui/rtext/RText'

type Props = DispatchProps & StateProps

const Navigatetab: React.FC<Props> = ({ tab, currentTab }) => {
  return (
    <Container>
      <TouchableWithoutFeedback
        onPress={() => {
          tab(1)
        }}
      >
        <View style={{ marginLeft: 15 }}>
          <RText
            text={TAB_NAMES.TABA}
            family="Oswald-SemiBold"
            size={
              currentTab === 1
                ? FONTS_STATUS.ACTIVE_SIZE
                : FONTS_STATUS.DESACTIVE_SIZE
            }
            color={
              currentTab === 1
                ? FONTS_STATUS.ACTIVE_COLOR
                : FONTS_STATUS.DESACTIVE_COLOR
            }
          />
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() => {
          tab(2)
        }}
      >
        <View style={{ marginLeft: 20 }}>
          <RText
            text={TAB_NAMES.TABB}
            family="Oswald-SemiBold"
            size={
              currentTab === 2
                ? FONTS_STATUS.ACTIVE_SIZE
                : FONTS_STATUS.DESACTIVE_SIZE
            }
            color={
              currentTab === 2
                ? FONTS_STATUS.ACTIVE_COLOR
                : FONTS_STATUS.DESACTIVE_COLOR
            }
          />
        </View>
      </TouchableWithoutFeedback>
    </Container>
  )
}

export default Navigatetab

/* import Icon from 'react-native-vector-icons/FontAwesome'
<Icon name="rocket" size={30} color="#900" /> */
