export interface StateProps {
  dispatch?(): void
  currentTab: number
}

export interface DispatchProps {
  tab(index: number): void
}
