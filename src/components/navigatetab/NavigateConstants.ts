import { primaryColor, textPrimary } from 'components/styles/Colors'

export enum TAB_NAMES {
  TABA = 'Criados',
  TABB = 'Conectados'
}

export const FONTS_STATUS = {
  ACTIVE_SIZE: 30,
  ACTIVE_COLOR: primaryColor,

  DESACTIVE_SIZE: 24,
  DESACTIVE_COLOR: textPrimary
}
