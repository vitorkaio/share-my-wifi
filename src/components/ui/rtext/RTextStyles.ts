import styled from 'styled-components/native'

interface Props {
  size: string
  family: string
  color: string
  weight: string
}

export const Text = styled.Text`
  font-size: ${(props: Props) => props.size};
  color: ${(props: Props) => props.color};
  font-family: ${(props: Props) => props.family};
  font-weight: ${(props: Props) => props.weight};
`
