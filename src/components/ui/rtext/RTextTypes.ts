export interface StateProps {
  family?: string
  size?: number
  weight?: string
  color?: string
  text: string
}

export interface DispatchProps {
  dispatch?(): void
}
