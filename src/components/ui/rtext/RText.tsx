import React from 'react'

import { Text } from './RTextStyles'
import { StateProps, DispatchProps } from './RTextTypes'

type Props = DispatchProps & StateProps

const RText: React.FC<Props> = ({ text, family, size, weight, color }) => {
  return (
    <Text
      family={family || 'OpenSans-Regular'}
      size={size ? `${size}px` : '14px'}
      weight={weight || '400'}
      color={color || 'black'}
    >
      {text}
    </Text>
  )
}

export default RText
