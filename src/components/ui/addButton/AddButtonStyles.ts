import styled from 'styled-components/native'
import { primaryColor } from 'components/styles/Colors'

export const Container = styled.TouchableOpacity`
  width: 53px;
  height: 53px;
  border-width: 1px;
  border-radius: 53px;
  border-color: ${primaryColor};
  background-color: ${primaryColor};
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 25px;
  bottom: 25px;
`
