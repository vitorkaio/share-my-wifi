import React from 'react'

import { Container } from './AddButtonStyles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { textPrimary } from 'components/styles/Colors'
import { DispatchProps } from './AddButtonTypes'

const Add: React.FC<DispatchProps> = props => {
  const { onPressHandler } = props
  return (
    <Container onPress={onPressHandler}>
      <Icon name="plus-thick" size={24} color={textPrimary} />
    </Container>
  )
}

export default Add
