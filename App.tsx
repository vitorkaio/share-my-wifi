import React from 'react'
import { StatusBar } from 'react-native'
import { backgroundColor } from 'components/styles/Colors'

import Home from 'pages/home/Home'

const App: React.FC = () => {
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={backgroundColor} />
      <Home />
    </>
  )
}

export default App
